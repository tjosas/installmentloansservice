﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace InstallmentLoansTestGuiClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder resultText = new StringBuilder();
            //double _loanValue, System.DateTime _payoutDate, System.DateTime _firstPaymentDate, int _installments, double _intrest
            //Check boxes
            bool validated = true;
            double dblLoanAmount = 0;
            
            if (String.IsNullOrWhiteSpace(loanAmount.Text) || (!double.TryParse(loanAmount.Text, out dblLoanAmount)))
            {
                validated = false;
                resultText.AppendLine("The loan amount is empty or not a number");
            }
            double dblIntrest = 0;
            if (String.IsNullOrWhiteSpace(intrest.Text) || (!double.TryParse(intrest.Text, out dblIntrest)))
            {
                validated = false;
                resultText.AppendLine("The Intrest is empty or not a number");
            }
            int intPayments = 0;
            if (String.IsNullOrWhiteSpace(payments.Text) || (!Int32.TryParse(payments.Text, out intPayments)))
            {
                validated = false;
                resultText.AppendLine("The payments are empty or not a number");
            }
            if (payoutDate.SelectedDate == null || payoutDate.SelectedDate.GetValueOrDefault() == DateTime.MinValue)
            {
                validated = false;
                resultText.AppendLine("The Payout Date is not set");
            }
            if (startDate.SelectedDate == null || startDate.SelectedDate.GetValueOrDefault() == DateTime.MinValue || startDate.SelectedDate < payoutDate.SelectedDate)
            {
                validated = false;
                resultText.AppendLine("The Start Date is not set or is before the start date");
            }

            if (validated)
            {
                var x = InstallmentLoansCalculator.Calculator.getCalculatedPaymentPlanDays(dblLoanAmount, (DateTime)payoutDate.SelectedDate, (DateTime)startDate.SelectedDate, intPayments, dblIntrest);
                resultText.AppendLine("You have to pay " + x.Item1);
                resultText.AppendLine("you have to pay on these dates:");
                double total = 0;
                foreach (Tuple<string, double> item in x.Item2)
                {
                    resultText.AppendLine(item.Item1 + ": " + item.Item2);
                    total += item.Item2;
                }
                resultText.AppendLine("You have to pay a total of " + total);
            }
            resultBox.Text = resultText.ToString();
        }
    }
}
