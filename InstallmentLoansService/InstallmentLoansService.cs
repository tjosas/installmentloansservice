﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstallmentLoansService
{
    public class InstallmentLoansService:IInstallmentLoansService
    {

        public string GetMessage(string name)
        {
            return "Hello world from " + name + "!";
        }

        public string GetInstallmentPlan(double _loanValue, DateTime _payoutDate, DateTime _firstPaymentDate, int _installments, double _intrest)
        {
            Tuple<double, List<Tuple<String, double>>> values = InstallmentLoansCalculator.Calculator.getCalculatedPaymentPlanDays( _loanValue, _payoutDate, _firstPaymentDate, _installments, _intrest);
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            return javaScriptSerializer.Serialize(values);

        }
    }
}
