﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace InstallmentLoansService
{
    [ServiceContract]
    interface IInstallmentLoansService
    {
        [OperationContract]
        string GetMessage(string name);

        [OperationContract]
        string GetInstallmentPlan(double _loanValue, DateTime _payoutDate, DateTime _firstPaymentDate, int _installments, double _intrest);
    }
}
