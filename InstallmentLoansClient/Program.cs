﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ServiceModel;
using System.Web.Script.Serialization;

namespace InstallmentLoansClient
{
    class Program
    {
        static void Main(string[] args)
        {
            InstallmentLoansServiceClient client = new InstallmentLoansServiceClient();
            Console.WriteLine(client.GetMessage("Skjera"));
            Console.Write("Webservice result: ");
            Console.WriteLine(client.GetInstallmentPlan(900, DateTime.Today, DateTime.Today.AddDays(30), 3, 0.008));
            Console.WriteLine("");
            Console.WriteLine("Direct result: ");
            DateTime payout = new DateTime(2015,04,01);
            DateTime startdate = new DateTime(2015,04,30);
            var x = InstallmentLoansCalculator.Calculator.getCalculatedPaymentPlanDays(900, payout, startdate, 3, 0.008);
            Console.WriteLine(x);
            Console.WriteLine("");
            var javaScriptSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
            string jsonString = javaScriptSerializer.Serialize(x);
            Console.WriteLine(jsonString);

            Console.WriteLine("");
            Console.WriteLine("");

            Console.WriteLine("You have to pay " + x.Item1);
            Console.WriteLine("you have to pay on these dates:");
            double total = 0;
            foreach (Tuple<string,double> item in x.Item2)
            {
                Console.WriteLine(item.Item1 +": " + item.Item2);
                total += item.Item2;
            }
            Console.WriteLine("You have to pay a total of " + total);
            
            Console.ReadKey();
        }
    }
}
