﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InstallmentLoansCalculator
{
    public static class Calculator
    {
        public static Tuple<double, List<Tuple<String, double>>> getCalculatedPaymentPlanDays(double _loanValue, DateTime _payoutDate, DateTime _firstPaymentDate, int _installments, double _intrest)
        {
            //Calculate total amount of days
            //DateTime enddate = _firstPaymentDate.AddMonths(_installments);
            //double totalDays = (enddate - _payoutDate).TotalDays;
            DateTime enddate = _payoutDate.AddMonths(_installments);
            double totalDays = (enddate - _payoutDate).TotalDays;
            //totalDays = 90;

            double payment = getPaymentValue(_loanValue, totalDays, _installments, _intrest);

            installmentObject installments = new installmentObject(payment, _loanValue, _loanValue, _intrest, _payoutDate, _firstPaymentDate, (int)totalDays);

            Tuple<double, List<Tuple<String, double>>> returnObject = new Tuple<double, List<Tuple<String, double>>>(payment, installments.PaymentList);

            return returnObject;
        }

        private static double getPaymentValue( double _loanValue, double _totalDays, int _installments, double _intrest)
        {
            /* First calculate how much each payment would be if each payment came in on time.
            * Using the formula: M = Pi/[q{1-[1+(i/q)]-nq}]
            * M = Payment value
            * i = intrest
            * n = years (or time in general)
            * q = payments pr n
            * A  = amount owed
            * k = payments
            * P = principal amount (loan value)
            * Mnq = total amount 
            * I = total amount ofintrest
            */

            double q = _installments / _totalDays;
            double Pi = _loanValue * _intrest;
            double nq = -1 * (_totalDays * q);
            double intIns = _intrest / q;
            double pow = Math.Pow(1 + intIns, nq);
            double M = Pi / (q * (1 - (pow)));

            return Math.Round(M,2);
        }
    }

    class installmentObject
    {
        public double Intrest
        {
            get;
            set;
        }

        public double Principal
        {
            get;
            set;
        }

        public double BasePrincipal
        {
            get;
            set;
        }

        public double Payment
        {
            get;
            set;
        }

        public List<Tuple<String, double>> PaymentList
        {
            get;
            set;
        }

        public installmentObject(double _payment, double _basePrincipal, double _principal, double _intrest, DateTime _payoutDate, DateTime _firstPaymentDate, int _totalDays)
        {
            Payment = _payment;
            BasePrincipal = _basePrincipal;
            Principal = _principal;
            Intrest = _intrest;
            CalculatePaymentSchedule(_payoutDate, _firstPaymentDate, _totalDays);
            //CalculatePaymentScheduleTest(_payoutDate, _firstPaymentDate);
        }

        private void CalculatePaymentSchedule(DateTime _payoutDate, DateTime _firstPaymentDate, int _totalDays)
        {
            String datePrefix = "dd-MM-yyyy";
            List<Tuple<String, double>> returnList = new List<Tuple<String, double>>();
            DateTime targetDate = _firstPaymentDate;

            List<DateTime> dates = new List<DateTime>();
            for (int i = 0; i < _totalDays; i++)
            {
                dates.Add(_payoutDate.AddDays(i));
            }

            Double[] accumulatedLoan = new double[_totalDays + 10];
            accumulatedLoan[0] = Principal;

            int counter = 0;
            foreach (DateTime item in dates)
            {
                if (counter != 0)
                {
                    accumulatedLoan[counter] = accumulatedLoan[counter - 1] + (BasePrincipal * Intrest);
                    if (item == targetDate)
                    {
                        if (Payment >= accumulatedLoan[counter])
                        {
                            Tuple<String, double> tmpTuple = new Tuple<String, double>(targetDate.ToString(datePrefix), accumulatedLoan[counter]);
                            returnList.Add(tmpTuple);
                            break;
                        }
                        else
                        {
                            double intrestAccumulated = accumulatedLoan[counter] - BasePrincipal;
                            double tobesubtractedfrombase = Payment - intrestAccumulated;
                            BasePrincipal = BasePrincipal - tobesubtractedfrombase;
                            accumulatedLoan[counter] = accumulatedLoan[counter] - Payment;
                            Tuple<String, double> tmpTuple = new Tuple<String, double>(targetDate.ToString(datePrefix), Payment);
                            returnList.Add(tmpTuple);
                            targetDate = targetDate.AddMonths(1);
                        }
                    }
                }
                counter++;
            }

            PaymentList = returnList;
        }

        private void CalculatePaymentScheduleTest(DateTime _payoutDate, DateTime _firstPaymentDate)
        {
            int _totalDays = 90;
            List<Tuple<DateTime, double>> returnList = new List<Tuple<DateTime, double>>();
            DateTime targetDate = _firstPaymentDate;

            List<DateTime> dates = new List<DateTime>();
            for (int i = 0; i < _totalDays; i++)
            {
                dates.Add(_payoutDate.AddDays(i));
            }

            Double[] y = new double[_totalDays + 10];
            y[0] = Principal;

            int counter = 1;
            foreach (DateTime item in dates)
            {
                y[counter] = y[counter - 1] + (BasePrincipal * Intrest);
                if (counter == 30 || counter == 60 || counter == 90 )
                {
                    if (Payment >= y[counter])
                    {
                        Tuple<DateTime, double> tmpTuple = new Tuple<DateTime, double>(targetDate, y[counter]);
                        returnList.Add(tmpTuple);
                        break;
                    }
                    else
                    {
                        double intrestAccumulated = y[counter] - BasePrincipal;
                        //Console.WriteLine("intrestAccumulated: " + intrestAccumulated);
                        double tobesubtractedfrombase = Payment - intrestAccumulated;
                        //Console.WriteLine("tobesubtractedfrombase: " + tobesubtractedfrombase);
                        BasePrincipal = BasePrincipal - tobesubtractedfrombase;
                        //Console.WriteLine("New basePrincipal: " + BasePrincipal);
                        y[counter] = y[counter] - Payment;
                        Tuple<DateTime, double> tmpTuple = new Tuple<DateTime, double>(targetDate, Payment);
                        returnList.Add(tmpTuple);
                        targetDate = targetDate.AddMonths(1);
                    }
                }
                counter++;
            }

            Console.WriteLine("");
            Console.WriteLine("Payments");
            foreach (Tuple<DateTime, double> item in returnList)
            {
                Console.WriteLine("Date: " + item.Item1 + "  Amount: " + item.Item2);
            }

            Console.WriteLine("");
            Console.WriteLine("Liste");
            counter = 0;
            foreach (double item in y)
            {
                Console.WriteLine("dag " + counter + ": " +item);
                counter++;
            }
        }
    }
}
